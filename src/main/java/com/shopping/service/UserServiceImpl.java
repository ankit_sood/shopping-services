package com.shopping.service;

import com.shopping.db.User;
import com.shopping.db.UserDAO;
import com.shopping.stubs.user.Gender;
import com.shopping.stubs.user.UserRequest;
import com.shopping.stubs.user.UserResponse;
import com.shopping.stubs.user.UserServiceGrpc;
import io.grpc.stub.StreamObserver;

public class UserServiceImpl extends UserServiceGrpc.UserServiceImplBase {
    @Override
    public void getUserDetails(UserRequest request, StreamObserver<UserResponse> responseObserver) {
        UserDAO userDAO = new UserDAO();
        User user = userDAO.getDetails(request.getUsername());

        UserResponse.Builder userResponseBuilder = UserResponse.newBuilder().setId(user.getId())
                    .setName(user.getName())
                    .setAge(user.getAge())
                    .setGender(Gender.valueOf(user.getGender()))
                    .setUsername(user.getUsername());

        UserResponse userResponse = userResponseBuilder.build();

        responseObserver.onNext(userResponse);
        responseObserver.onCompleted();
    }
}
