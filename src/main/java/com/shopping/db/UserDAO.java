package com.shopping.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAO {
    private static final Logger logger = Logger.getLogger(UserDAO.class.getName());

    public User getDetails(String username){
        User user = new User();
        try{
            Connection conn = H2DatabaseConnection.getConnectionToDatabase();
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM USER WHERE username = ?");
            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                user.setId(rs.getInt("id"));
                user.setAge(rs.getInt("age"));
                user.setGender(rs.getString("gender"));
                user.setName(rs.getString("name"));
                user.setUsername(rs.getString("username"));
            }
        } catch (Exception ex){
            logger.log(Level.SEVERE, "Could not execute the query. ", ex);
        }
        return user;
    }
}
